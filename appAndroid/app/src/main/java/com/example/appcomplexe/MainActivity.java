package com.example.appcomplexe;

import androidx.appcompat.app.AppCompatActivity;

import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;

import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.provider.OpenableColumns;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import java.util.concurrent.ExecutionException;

/**
 * Application permettant à l'utilisateur de désactiver/activer son bluetooth, de choisir un fichier et l'envoyer sur une futur API
 */
public class MainActivity extends AppCompatActivity {

    private static final int REQUEST_ENABLE_BT = 0;

    private static final String BLUETOOTH_ON = "Bluetooth activé";
    private static final String BLUETOOTH_OFF = "Bluetooth désactivé";
    private static final String BLUETOOTH_REFUSE = "L'utilisateur a refusé d'activer le bluetooth";

    private static final String ARG_0 = "ARG0";
    private static final String CANT_OBTAIN_FILE_NAME = "Impossible d'obtenir le nom du fichier, " + ARG_0 + " est nul";

    private static final String FILE_CHOOSE = "Fichier choisi : ";

    private TextView textViewFileName;
    private ImageView imageView;
    private Button btnActivatedBluetooth, btnDeactivatedBluetooth, btnGetFile, btnSendFile;

    private BluetoothAdapter mBlueAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textViewFileName = findViewById(R.id.textView_FileName);
        imageView = findViewById(R.id.imageView);

        btnActivatedBluetooth = findViewById(R.id.onBtn);
        btnDeactivatedBluetooth = findViewById(R.id.offBtn);
        btnGetFile = findViewById(R.id.button_GetFile);
        btnSendFile = findViewById(R.id.button_SendFile);
        // Désactivation du bouton d'envoi de fichier
        btnSendFile.setEnabled(false);

        mBlueAdapter = BluetoothAdapter.getDefaultAdapter();

        //On définit l'image en fonction de l'état Bluetooth (activé / désactivé)
        if (mBlueAdapter.isEnabled()){
            imageView.setImageResource(R.drawable.ic_action_on);
            viewButtonOffBluetooth();
        } else {
            imageView.setImageResource(R.drawable.ic_action_off);
            viewButtonOnBluetooth();
        }

        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

        // lors du clic sur le bouton d'activation du bluetooth, on demande l'autorisation  pour activer le bluetooth
        btnActivatedBluetooth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!mBlueAdapter.isEnabled()){
                    //on lance l'intend
                    Intent intent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                    startActivityForResult(intent, REQUEST_ENABLE_BT);
                }
            }
        });

        // lors du clic sur le bouton de désactivation du bluetooth, on demande l'autorisation  pour désactiver le bluetooth
        btnDeactivatedBluetooth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mBlueAdapter.isEnabled()){
                    mBlueAdapter.disable();
                    imageView.setImageResource(R.drawable.ic_action_off);
                    viewButtonOnBluetooth();
                    showToast(BLUETOOTH_OFF);
                }
            }
        });

        // lors du clic sur le bouton d'envoi du fichier, on contacte notre api et on renvoi le resultat
        btnSendFile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String resultAPI = null;
                try {
                    // on exécute le post sur l'api et on récupère le résultat
                    resultAPI = new HttpPostRequest().execute().get();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                // on affiche le resultat
                showToast(resultAPI);
            }
        });
    }

    /**
     * Affichage l'image bluetooth 'off'
     */
    private void viewButtonOffBluetooth() {
        btnDeactivatedBluetooth.setVisibility(View.VISIBLE);
        btnActivatedBluetooth.setVisibility(View.GONE);
    }

    /**
     * Affichage l'image bluetooth 'on'
     */
    private void viewButtonOnBluetooth() {
        btnDeactivatedBluetooth.setVisibility(View.GONE);
        btnActivatedBluetooth.setVisibility(View.VISIBLE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode){
            case 1001:
                if(data.getData() == null) {
                    throw new IllegalArgumentException(CANT_OBTAIN_FILE_NAME.replace(ARG_0, "data"));
                }
                //Obtenir un curseur avec des informations concernant cet uri
                Cursor cursor = getContentResolver().query(data.getData(), null, null, null, null);
                if (cursor.getCount() <= 0) {
                    cursor.close();
                    throw new IllegalArgumentException(CANT_OBTAIN_FILE_NAME.replace(ARG_0, "cursor"));
                }
                cursor.moveToFirst();
                // on récupère le nom du fichier et on l'affiche
                String fileName = cursor.getString(cursor.getColumnIndexOrThrow(OpenableColumns.DISPLAY_NAME));
                String textTvFileName = FILE_CHOOSE + fileName;
                textViewFileName.setText(textTvFileName);
                // on active le bouton send file
                btnSendFile.setEnabled(true);
                break;
            case REQUEST_ENABLE_BT:
                if (resultCode == RESULT_OK){    //bluetooth activé
                    imageView.setImageResource(R.drawable.ic_action_on);
                    viewButtonOffBluetooth();
                    showToast(BLUETOOTH_ON);
                } else { //l'utilisateur à refusé le bluetooth
                    showToast(BLUETOOTH_REFUSE);
                }
                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    public void getFile(View v) {
        // on lance l'intent pour choisir un fichier
        Intent mediaIntent = new Intent(Intent.ACTION_GET_CONTENT);
        mediaIntent.setType("*/*");
        mediaIntent.addCategory(Intent.CATEGORY_OPENABLE);
        startActivityForResult(mediaIntent, 1001);
    }

    //affiche le message toast
    private void showToast(String msg){
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }
}