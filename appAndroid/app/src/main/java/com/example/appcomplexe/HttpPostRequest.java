package com.example.appcomplexe;

import android.os.AsyncTask;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

/**
 * Permets de faire une request http post
 */
public class HttpPostRequest extends AsyncTask<String, Void, String> {

    /**
     * Permets de faire un test d'API
     */
    @Override
    protected String doInBackground(String... params){
        String urlString = "https://postman-echo.com/post";
        String dataAtSend = "test post api postman";
        StringBuilder response = new StringBuilder();
        try {
            URL url = new URL(urlString);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setReadTimeout(10000);
            urlConnection.setConnectTimeout(15000);
            urlConnection.setRequestMethod("POST");
            OutputStream out = new BufferedOutputStream(urlConnection.getOutputStream());

            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(out, "UTF-8"));
            writer.write(dataAtSend);
            writer.flush();
            writer.close();
            out.close();
            urlConnection.connect();

            int responseCode = urlConnection.getResponseCode();
            if (responseCode == HttpsURLConnection.HTTP_OK) {
                String line;
                BufferedReader br=new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                while ((line=br.readLine()) != null) {
                    response.append(line);
                }
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return response.toString();
    }
    @Override
    protected void onPostExecute(String result){
        super.onPostExecute(result);
    }
}