import com.fasterxml.jackson.databind.ObjectMapper;
import model.Position;
import model.User;

import javax.bluetooth.*;
import javax.microedition.io.Connector;
import javax.obex.ClientSession;
import javax.obex.HeaderSet;
import javax.obex.Operation;
import javax.obex.ResponseCodes;
import java.io.OutputStream;
import java.net.InetAddress;
import java.util.ArrayList;

public class SendDataOnPhonePerBluetooth implements DiscoveryListener {

    private static final String CHARSET = "iso-8859-1";

    private static final String EXTENSION_FILE = ".txt";
    private static final String FILE_NAME = "covid19";

    public static final String CONNECTION = "connexion";
    public static final String CONNECTION_FAILED = "Échec de "+CONNECTION;

    public static final String FOUND = " trouvé ";
    public static final String SERVICE_FOUND = "Service " + FOUND + ":";
    public static final String SERVICE_WITH_NAME = "Service avec le nom ";
    public static final String OBEX_OBJECT_PUSH = "OBEX Object Push\u0000";

    private static Object lock=new Object();
    public ArrayList<RemoteDevice> devices;

    public SendDataOnPhonePerBluetooth() {
        devices = new ArrayList<RemoteDevice>();
    }

    public static void main(String[] args) {

        final SendDataOnPhonePerBluetooth listener =  new SendDataOnPhonePerBluetooth();

        try{
            final LocalDevice localDevice = LocalDevice.getLocalDevice();
            final DiscoveryAgent agent = localDevice.getDiscoveryAgent();
            agent.startInquiry(DiscoveryAgent.GIAC, listener);
            try {
                synchronized(lock){
                    lock.wait();
                }
            }
            catch (InterruptedException e) {
                e.printStackTrace();
                return;
            }

            UUID[] uuidSet = new UUID[1];
            uuidSet[0]=new UUID(0x1105);

            int[] attrIDs =  new int[] {
                    0x0100
            };

            for (RemoteDevice device : listener.devices) {
                agent.searchServices(
                        attrIDs,uuidSet,device,listener);
                try {
                    synchronized(lock){
                        lock.wait();
                    }
                }
                catch (InterruptedException e) {
                    e.printStackTrace();
                    return;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }



    @Override
    public void deviceDiscovered(RemoteDevice remoteDevice, DeviceClass deviceClass) {
        String name;
        try {
            name = remoteDevice.getFriendlyName(false);
        } catch (Exception e) {
            name = remoteDevice.getBluetoothAddress();
        }
        devices.add(remoteDevice);
        System.out.println("Appareil " + FOUND + ": " + name);
    }

    @Override
    public void inquiryCompleted(int arg0) {
        synchronized(lock){
            lock.notify();
        }
    }

    @Override
    public void serviceSearchCompleted(int arg0, int arg1) {
        synchronized (lock) {
            lock.notify();
        }
    }

    @Override
    public void servicesDiscovered(int transID, ServiceRecord[] serviceRecords) {
        for (int i = 0; i < serviceRecords.length; i++) {
            // On récupère l'adresse mac et si elle est null, on parcourt un autre service
            String url = serviceRecords[i].getConnectionURL(ServiceRecord.NOAUTHENTICATE_NOENCRYPT, false);
            if (url == null) {
                continue;
            }
            // On récupère le service
            DataElement serviceName = serviceRecords[i].getAttributeValue(0x0100);
            // Si "service name" connu, on envoie un message si la valeur du service est égale "OBEX_OBJECT_PUSH" sinon on alerte simplement qu'on à trouvé un service
            if (serviceName != null) {
                System.out.println(SERVICE_WITH_NAME + serviceName.getValue() + FOUND + url);
                if(serviceName.getValue().equals(OBEX_OBJECT_PUSH)){
                    sendDataToDevice(url);
                }
            } else {
                System.out.println(SERVICE_FOUND + url);
            }
        }
    }

    /**
     * Envoi des données au téléphone connecté
     * @param urlDevice adresse MAC
     */
    private static void sendDataToDevice(String urlDevice){
        try{
            System.out.println(CONNECTION + urlDevice);
            //Tentative de connexion
            ClientSession clientSession = (ClientSession) Connector.open(urlDevice);
            HeaderSet hsConnectReply = clientSession.connect(null);
            // Si on n'arrive pas à se connecter, on quitte l'envoi de messages et on préviens l'utilisateur qu'on ne peut pas se connecter
            if (hsConnectReply.getResponseCode() != ResponseCodes.OBEX_HTTP_OK) {
                System.out.println(CONNECTION_FAILED);
                return;
            }
            //On créer l'header
            HeaderSet hsOperation = clientSession.createHeaderSet();
            hsOperation.setHeader(HeaderSet.NAME, FILE_NAME + EXTENSION_FILE);
            hsOperation.setHeader(HeaderSet.DESCRIPTION, FILE_NAME);
            //Création de l'opération
            Operation putOperation = clientSession.put(hsOperation);
            //Envoie les données au client
            User user = new User(InetAddress.getLocalHost().getHostName());
            user.addPosition(new Position(1, 200));
            user.addPosition(new Position(2, 200));
            ObjectMapper mapper = new ObjectMapper();
            String jsonString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(user);
            //Envoi des données
            byte data[] = jsonString.getBytes(CHARSET);
            OutputStream os = putOperation.openOutputStream();
            os.write(data);
            //Fermeture
            os.close();
            putOperation.close();
            clientSession.disconnect(null);
            clientSession.close();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

}